from pyspark import SparkConf
from pyspark.sql import SparkSession
import argparse


def main(year: int, schema: str, output_path: str) -> None:
    """
    Combines all the input metrics into one dataframe and saves it to HDFS.

    param int year: year
    param str schema: schema
    param str output_path: output path

    e.g.  /path/to/equity_landscape_input_metrics.py --year 2021 --schema gdi --output_path /path/to/output/

    """

    conf = SparkConf()
    conf.setAppName("equity_landscape_input_metrics")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()
    df = spark.sql(
        f"""
        WITH country_data AS (
            SELECT distinct iso3_country_code                                                                                               iso3_country_code,
                    first_value(country_area_label) over(PARTITION BY iso3_country_code)                                                    country_name,
                    iso2_country_code                                                                                                       iso2_country_code,
                    un_continent_description                                                                                                continent,
                    un_subcontinent_description                                                                                             sub_continent
                FROM {schema}.country_meta_data country
                WHERE iso3_country_code IS NOT NULL
            )
        SELECT 'country'                                                                                                                    geography_level,
                country.iso3_country_code                                                                                                   iso3_country_code,
                country.iso2_country_code                                                                                                   iso2_country_code,
                country.country_name                                                                                                        country_name,
                country.continent                                                                                                           continent,
                country.sub_continent                                                                                                       sub_continent,
                affiliate.count_operating_affiliates                                                                                        count_operating_affiliates,
                affiliate.affiliate_size_max                                                                                                affiliate_size_max,
                round(affiliate.affiliate_tenure_max,2)                                                                                     affiliate_tenure_max,
                round(grants.sum_calendar_year_affiliate_grants/grants.sum_calendar_year_grants,2)                                          affiliate_percent_annual_grants,
                round(grants.sum_historical_affiliate_grants/grants.sum_historical_grants_to_date,2)                                        affiliate_percent_historical_grants,
                round(grants.sum_historical_affiliate_grants,2)                                                                             sum_historical_affiliate_grants,
                round(affiliate.affiliate_tenure_average,2)                                                                                 affiliate_tenure_average,
                affiliate.governance_type                                                                                                   affiliate_governance_type,
                affiliate.affiliate_size_growth                                                                                             affiliate_size_growth,
                round(grants.sum_historical_grants_to_date,2)                                                                               sum_historical_grants_to_date,
                round(grants.sum_calendar_year_grants,2)                                                                                    sum_calendar_year_grants,
                grants.count_historical_grants_to_date                                                                                      count_historical_grants_to_date,
                grants.count_calendar_year_grants                                                                                           count_calendar_year_grants,
                round(grants.annual_grants_weighted,2)                                                                                      annual_grants_weighted,
                round(grants.annual_affiliate_grants_weighted,2)                                                                            annual_affiliate_grants_weighted,
                round(grants.historical_grants_weighted,2)                                                                                  historical_grants_weighted,
                round(grants.historical_affiliate_grants_weighted,2)                                                                        historical_affiliate_grants_weighted,
                round(grants.annual_grants_change_rate,2)                                                                                   annual_grants_change_rate,
                round(grants.annual_grants_by_annual_change_rate,2)                                                                         annual_grants_by_annual_change_rate,
                round(grants.historical_grants_by_annual_change_rate,2)                                                                     historical_grants_by_annual_change_rate,
                round(grants.sum_calendar_year_affiliate_grants,2)                                                                          sum_calendar_year_affiliate_grants,
                grants.count_calendar_year_affiliate_grants                                                                                 count_calendar_year_affiliate_grants,
                grants.count_historical_affiliate_grants                                                                                    count_historical_affiliate_grants,
                round(pivot_yoy_change.commons,2)                                                                                           commons_annual_change,
                round(pivot_yoy_change.mediawiki,2)                                                                                         mediawiki_annual_change,
                round(pivot_yoy_change.wikidata,2)                                                                                          wikidata_annual_change,
                round(pivot_yoy_change.wikipedia,2)                                                                                         wikipedia_annual_change,
                round(pivot_yoy_change.wikisource,2)                                                                                        wikisource_annual_change,
                round(pivot_yoy_change.sister_project,2)                                                                                    sister_projects_annual_change,
                round(pivot_yoy_change.organizing_wiki,2)                                                                                   organizing_wiki_annual_change,
                round(pivot_monthly_bins.commons,2)                                                                                         commons_annual_signal,
                round(pivot_monthly_bins.mediawiki,2)                                                                                       mediawiki_annual_signal,
                round(pivot_monthly_bins.wikidata,2)                                                                                        wikidata_annual_signal,
                round(pivot_monthly_bins.wikipedia,2)                                                                                       wikipedia_annual_signal,
                round(pivot_monthly_bins.wikisource,2)                                                                                      wikisource_annual_signal,
                round(pivot_monthly_bins.sister_project,2)                                                                                  sister_projects_annual_signal,
                round(pivot_monthly_bins.organizing_wiki,2)                                                                                 organizing_wiki_annual_signal,
                round(online_metrics.percent_editors_active,2)                                                                              percent_editors_active_month,
                pageviews.metric_value                                                                                                      pageviews_annual_signal,
                yoy_pageviews.metric_value                                                                                                  pageviews_annual_change,
                unique_devices.metric_value                                                                                                 unique_devices_annual_signal,
                yoy_devices.metric_value                                                                                                    unique_devices_annual_change,
                {year}                                                                                                                      year
            FROM country_data country
            LEFT JOIN  {schema}.affiliate_leadership_input_metrics affiliate ON (affiliate.year = {year} AND country.iso3_country_code = affiliate.country_code)
            LEFT JOIN  {schema}.grants_leadership_input_metrics grants ON (grants.year = {year} AND country.iso3_country_code = grants.country_code)
            LEFT JOIN  {schema}.geoeditor_input_metrics_pivot pivot_yoy_change ON (pivot_yoy_change.year = {year} AND pivot_yoy_change.metric = 'yoy_change' AND country.iso2_country_code = pivot_yoy_change.country_code)
            LEFT JOIN  {schema}.geoeditor_input_metrics_pivot pivot_monthly_bins ON (pivot_monthly_bins.year = {year} AND pivot_monthly_bins.metric = 'monthly_bins' AND country.iso2_country_code = pivot_monthly_bins.country_code)
            LEFT JOIN  {schema}.geoeditor_online_input_metrics online_metrics ON (online_metrics.year = {year} AND country.iso2_country_code = online_metrics.country_code)
            LEFT JOIN  {schema}.georeadership_input_metrics yoy_pageviews ON (yoy_pageviews.year = {year} AND yoy_pageviews.metric = 'yoy_pageviews' AND country.iso2_country_code = yoy_pageviews.country_code)
            LEFT JOIN  {schema}.georeadership_input_metrics pageviews ON (pageviews.year = {year} AND pageviews.metric = 'pageviews' AND country.iso2_country_code = pageviews.country_code)
            LEFT JOIN  {schema}.georeadership_input_metrics unique_devices ON (unique_devices.year = {year} AND unique_devices.metric = 'unique_devices' AND country.iso2_country_code = unique_devices.country_code)
            LEFT JOIN  {schema}.georeadership_input_metrics yoy_devices ON (yoy_devices.year = {year} AND yoy_devices.metric = 'yoy_unique_devices' AND country.iso2_country_code = yoy_devices.country_code)
    """
    )

    df = df.replace(float("nan"), None)

    df.write.partitionBy("year").mode("overwrite").parquet(output_path)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--schema", type=str, help="Hive schema to write to")
    parser.add_argument(
        "--year", type=str, help="Year for which metrics should be calculated for"
    )
    parser.add_argument("--output-path", type=str, help="Path to write output to")

    args = parser.parse_args()

    if args.year:
        year = int(args.year)
    else:
        raise ValueError("Year not specified")

    if args.schema:
        schema = args.schema
    else:
        raise ValueError("Schema not specified")

    if args.output_path:
        output_path = args.output_path
    else:
        raise ValueError("Output path not specified")

    main(year=year, schema=schema, output_path=output_path)
