from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import monotonically_increasing_id, lit
import argparse


def main(year: int, output_path: str, input_path: str) -> None:
    """
    Combines all the input metrics and save it to regional input metrics
    param int year: year
    param str output_path: output path
    param str input_path: input path

    e.g.  /path/to/equity_landscape_input_metrics.py --year 2021 --output_path /path/to/output/ --input_path /path/to/input/

    """

    conf = SparkConf()
    conf.setAppName("equity_landscape_regional_input_metrics")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # Read the input metrics
    spark.read.parquet(input_path).createOrReplaceTempView("input_metrics")

    df = spark.sql(
        """
            WITH regional_agg AS (
                    SELECT
                        geography_level AS geography_level_temp,
                        sub_continent,
                        continent,
                        global,
                        SUM(COALESCE(count_operating_affiliates, 0)) AS count_operating_affiliates,
                        MAX(COALESCE(affiliate_size_max, 0)) AS affiliate_size_max,
                        MAX(COALESCE(affiliate_tenure_max, 0)) AS affiliate_tenure_max,
                        AVG(COALESCE(affiliate_tenure_average, 0)) AS affiliate_tenure_average,
                        SUM(COALESCE(affiliate_percent_annual_grants, 0)) AS affiliate_percent_annual_grants,
                        SUM(COALESCE(affiliate_percent_historical_grants, 0)) AS affiliate_percent_historical_grants,
                        AVG(COALESCE(affiliate_governance_type, 0)) AS affiliate_governance_type,
                        SUM(COALESCE(count_historical_affiliate_grants, 0)) AS count_historical_affiliate_grants,
                        SUM(COALESCE(count_calendar_year_affiliate_grants, 0)) AS count_calendar_year_affiliate_grants,
                        SUM(COALESCE(sum_calendar_year_affiliate_grants, 0)) AS sum_calendar_year_affiliate_grants,
                        SUM(COALESCE(sum_historical_affiliate_grants, 0)) AS sum_historical_affiliate_grants,
                        SUM(COALESCE(annual_affiliate_grants_weighted, 0)) AS annual_affiliate_grants_weighted,
                        SUM(COALESCE(sum_historical_grants_to_date, 0)) AS sum_historical_grants_to_date,
                        SUM(COALESCE(historical_affiliate_grants_weighted, 0)) AS historical_affiliate_grants_weighted,
                        SUM(COALESCE(affiliate_size_growth, 0)) AS affiliate_size_growth,
                        SUM(COALESCE(sum_calendar_year_grants, 0)) AS sum_calendar_year_grants,
                        SUM(COALESCE(count_historical_grants_to_date, 0)) AS count_historical_grants_to_date,
                        SUM(COALESCE(count_calendar_year_grants, 0)) AS count_calendar_year_grants,
                        SUM(COALESCE(annual_grants_weighted, 0)) AS annual_grants_weighted,
                        SUM(COALESCE(historical_grants_weighted, 0)) AS historical_grants_weighted,
                        AVG(COALESCE(annual_grants_change_rate, 0)) AS annual_grants_change_rate,
                        AVG(COALESCE(annual_grants_by_annual_change_rate, 0)) AS annual_grants_by_annual_change_rate,
                        AVG(COALESCE(historical_grants_by_annual_change_rate, 0)) AS historical_grants_by_annual_change_rate,
                        AVG(COALESCE(commons_annual_change, 0)) AS commons_annual_change,
                        AVG(COALESCE(mediawiki_annual_change, 0)) AS mediawiki_annual_change,
                        AVG(COALESCE(wikidata_annual_change, 0)) AS wikidata_annual_change,
                        AVG(COALESCE(wikipedia_annual_change, 0)) AS wikipedia_annual_change,
                        AVG(COALESCE(wikisource_annual_change, 0)) AS wikisource_annual_change,
                        AVG(COALESCE(sister_projects_annual_change, 0)) AS sister_projects_annual_change,
                        AVG(COALESCE(organizing_wiki_annual_change, 0)) AS organizing_wiki_annual_change,
                        SUM(COALESCE(commons_annual_signal, 0)) AS commons_annual_signal,
                        SUM(COALESCE(mediawiki_annual_signal, 0)) AS mediawiki_annual_signal,
                        SUM(COALESCE(wikidata_annual_signal, 0)) AS wikidata_annual_signal,
                        SUM(COALESCE(wikipedia_annual_signal, 0)) AS wikipedia_annual_signal,
                        SUM(COALESCE(wikisource_annual_signal, 0)) AS wikisource_annual_signal,
                        SUM(COALESCE(sister_projects_annual_signal, 0)) AS sister_projects_annual_signal,
                        SUM(COALESCE(organizing_wiki_annual_signal, 0)) AS organizing_wiki_annual_signal,
                        SUM(COALESCE(percent_editors_active_month, 0)) AS percent_editors_active_month,
                        SUM(COALESCE(pageviews_annual_signal, 0)) AS pageviews_annual_signal,
                        AVG(COALESCE(pageviews_annual_change, 0)) AS pageviews_annual_change,
                        SUM(COALESCE(unique_devices_annual_signal, 0)) AS unique_devices_annual_signal,
                        AVG(COALESCE(unique_devices_annual_change, 0)) AS unique_devices_annual_change
                    FROM (
                            SELECT
                                *,
                                'global' AS global
                            FROM
                                input_metrics
                        )
                    WHERE
                        geography_level = 'country'
                    GROUP BY GROUPING SETS (
                        (geography_level, sub_continent),
                        (geography_level, continent),
                        (geography_level, global))),
                    regional_agg_stack AS (
                        SELECT
                            STACK(3, 'sub_continent', sub_continent, 'continent', continent, 'global', global) AS (geography_level, country_name),
                            agg.*
                        FROM
                            regional_agg agg
                    )
            SELECT *
            FROM regional_agg_stack
            WHERE country_name IS NOT NULL
    """
    )

    df = df.replace(float("nan"), 0)

    df = df.fillna(0)

    df = df.withColumn("iso3_country_code", df["country_name"])

    df = df.withColumn("iso2_country_code", df["country_name"])

    columns = [
        "geography_level",
        "iso3_country_code",
        "iso2_country_code",
        "country_name",
        "continent",
        "sub_continent",
    ]

    df_metrics_metadata = df.select(*columns).withColumn(
        "id", monotonically_increasing_id()
    )

    df_metrics_values = df.drop(*columns).withColumn(
        "id", monotonically_increasing_id()
    )

    df_regional_input_metrics = df_metrics_metadata.join(
        df_metrics_values, on="id", how="inner"
    ).drop("id")

    df_regional_input_metrics = df_regional_input_metrics.drop(
        "geography_level_temp", "global"
    )

    df_regional_input_metrics.createOrReplaceTempView("regional_input_metrics")

    df_countries = spark.sql(
        """
    SELECT *
    FROM input_metrics
    WHERE geography_level = 'country'
    ORDER BY country_name
    """
    )

    # Create the sub_continents DataFrame
    df_sub_continents = spark.sql(
        """
        SELECT *
        FROM regional_input_metrics
        WHERE geography_level = 'sub_continent'
        ORDER BY country_name
    """
    )

    # Create the continents DataFrame
    df_continents = spark.sql(
        """
        SELECT *
        FROM regional_input_metrics
        WHERE geography_level = 'continent'
        ORDER BY country_name
    """
    )

    # Create the global DataFrame
    df_global = spark.sql(
        """
        SELECT *
        FROM regional_input_metrics
        WHERE geography_level = 'global'
        ORDER BY country_name
    """
    )

    df_regional_input_metrics = (
        df_countries.unionByName(df_sub_continents)
        .unionByName(df_continents)
        .unionByName(df_global)
    )

    df_regional_input_metrics = df_regional_input_metrics.withColumn("year", lit(year))

    df_regional_input_metrics.write.partitionBy("year").mode("overwrite").parquet(
        output_path
    )


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--year", type=str, help="Year for which metrics should be calculated for"
    )
    parser.add_argument("--output-path", type=str, help="Path to write output to")

    parser.add_argument("--input-path", type=str, help="Path to read input from")

    args = parser.parse_args()

    if args.year:
        year = int(args.year)
    else:
        raise ValueError("Year not specified")

    if args.output_path:
        output_path = args.output_path
    else:
        raise ValueError("Output path not specified")

    if args.input_path:
        input_path = args.input_path
    else:
        raise ValueError("Input path not specified")

    input_path = f"{input_path}/year={year}"

    main(year=year, output_path=output_path, input_path=input_path)
