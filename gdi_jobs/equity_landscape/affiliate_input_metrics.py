import argparse

from gdi_jobs.util import convert_data_types

from pyspark import SparkConf
from pyspark.sql import SparkSession, DataFrame

import pyspark.sql.functions as F
from pyspark.sql.types import (
    StructType,
    StructField,
    StringType,
    IntegerType,
    ArrayType,
    DateType,
    DoubleType,
)


"""
Program to run the affiliate input metrics job
"""


def _upper_case_column(df: DataFrame, column_name: str) -> DataFrame:
    if column_name in df.columns:
        return df.withColumn(column_name, F.upper(column_name))
    else:
        return df


def _process_args():
    """Processes command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--schema", help="Name of the table to be written", required=True
    )
    parser.add_argument(
        "--hdfs-path",
        help="HDFS location for csv files to be read",
        required=True,
    )

    parser.add_argument("--year", help="Year to be used as a partition", required=True)

    args = parser.parse_args()

    return args


def _get_schema_affiliates_by_geo() -> StructType:
    return StructType(
        [
            StructField("country_code", StringType(), True),
            StructField("affiliate_code", StringType(), True),
            StructField("affiliate_name", StringType(), True),
            StructField("affiliate_type", StringType(), True),
            StructField("member_count", IntegerType(), True),
            StructField("affiliate_size", IntegerType(), True),
            StructField("size_growth", IntegerType(), True),
            StructField("languages", ArrayType(StringType()), True),
            StructField("affiliate_start_date", DateType(), True),
            StructField("tenure", DoubleType(), True),
            StructField("primary_location", StringType(), True),
            StructField("governance_growth", StringType(), True),
            StructField("un_continent_region", StringType(), True),
            StructField("un_sub_continent_region", StringType(), True),
            StructField("status", StringType(), True),
            StructField("total_growth", IntegerType(), True),
            StructField("wmf_region", StringType(), True),
            StructField("region_class", StringType(), True),
        ]
    )


def _get_schema_affiliate_data() -> StructType:
    return StructType(
        [
            StructField("affiliate_code", StringType(), True),
            StructField("superset_affiliate_name", StringType(), True),
            StructField("affiliate_country", StringType(), True),
            StructField("languages", ArrayType(StringType()), True),
            StructField("region", StringType(), True),
            StructField("affiliate_type_2", StringType(), True),
            StructField("initial_affiliate_recognition_date", DateType(), True),
            StructField("tenure", DoubleType(), True),
            StructField("member_count", IntegerType(), True),
            StructField("affiliate_size", IntegerType(), True),
            StructField("affiliate_size_change", IntegerType(), True),
            StructField("overall_capacity", StringType(), True),
            StructField("governance_type", StringType(), True),
            StructField("governance_type_ordinal", DoubleType(), True),
            StructField("affiliate_country", StringType(), True),
            StructField("country_code", StringType(), True),
            StructField("board_member_perc_men", DoubleType(), True),
            StructField("board_member_perc_female", DoubleType(), True),
            StructField("board_member_perc_gender_minority", DoubleType(), True),
            StructField("perc_men", DoubleType(), True),
            StructField("perc_female", DoubleType(), True),
            StructField("perc_gender_minority", DoubleType(), True),
            StructField("perc_member_men", DoubleType(), True),
            StructField("perc_member_female", DoubleType(), True),
            StructField("perc_member_gender_minority", DoubleType(), True),
            StructField("community_member_count", IntegerType(), True),
            StructField("member_support", StringType(), True),
            StructField("member_lead", StringType(), True),
            StructField("cap_1", StringType(), True),
            StructField("cap_2", StringType(), True),
            StructField("cap_3", StringType(), True),
            StructField("prog_1", StringType(), True),
            StructField("prog_2", StringType(), True),
            StructField("prog_3", StringType(), True),
            StructField("wmf_grants_awareness", StringType(), True),
            StructField("affiliate_grants_awareness", StringType(), True),
            StructField("external_grants_awareness", StringType(), True),
            StructField("regional_body_1", StringType(), True),
            StructField("regional_body_2", StringType(), True),
            StructField("regional_body_3", StringType(), True),
            StructField("operating_countries", ArrayType(StringType()), True),
            StructField("change_in_governance_type_ordinal", IntegerType(), True),
            StructField("growth_change_points", IntegerType(), True),
        ]
    )


def _main(
    schema: str,
    hdfs_path: str,
    year: int,
) -> None:
    """
    Computes the affiliate input metrics and writes them to Hive

    param schema: Name of the schema to be written
    param hdfs_path: HDFS location for csv files to be read
    param partition_year: Year to be used as a partition
    """

    conf = SparkConf()
    conf.setAppName("affiliate_input_metrics")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    hdfs_path_affiliates_by_geo = f"{hdfs_path}/affiliates_by_geo_{year}.csv"

    # affiliate_data hdfs path for the current year
    hdfs_path_affiliate_data = f"{hdfs_path}/affiliate_data_{year}.csv"

    # Read the csv files into dataframes
    df_affiliates_by_geo = spark.read.csv(hdfs_path_affiliates_by_geo, header=True)

    df_affiliate_data = spark.read.csv(hdfs_path_affiliate_data, header=True)

    # get the schema for the dataframes
    schema_affiliates_by_geo = _get_schema_affiliates_by_geo()
    schema_affiliate_data = _get_schema_affiliate_data()

    # convert the data types
    df_affiliates_by_geo = convert_data_types(
        df_affiliates_by_geo, schema_affiliates_by_geo, date_format="YYYY-mm-dd"
    )

    df_affiliate_data = convert_data_types(
        df_affiliate_data, schema_affiliate_data, date_format="mm/dd/YYYY"
    )

    # upper case the country code
    df_affiliates_by_geo = _upper_case_column(df_affiliates_by_geo, "country_code")
    df_affiliate_data = _upper_case_column(df_affiliate_data, "country_code")

    df_affiliates_by_geo = df_affiliates_by_geo.where(
        " length(country_code) = 3 OR country_code = 'INT\\'L' "
    )

    df_affiliates_by_geo.createOrReplaceTempView("affialites_geo")
    df_affiliate_data.createOrReplaceTempView("affiliate_data")

    df = spark.sql(
        f"""
    WITH current_affiliates AS (
           SELECT curr.country_code           AS country_code,
                COUNT(1)                      AS count_affiliates_in_country,
                AVG(tenure)                   AS operating_affiliates_tenure_avg
           FROM affialites_geo curr
          GROUP BY curr.country_code
        ),
        aff_data AS (
            SELECT country_code              AS country_code,
                MAX(governance_type_ordinal) AS governance_type,
                SUM(growth_change_points)    AS affiliate_growth,
                MAX(tenure)                  AS affiliate_tenure_max,
                MAX(affiliate_size)          AS affiliate_max_size
            FROM affiliate_data
            GROUP BY country_code
        ),
        affiliates AS (
        SELECT curr.country_code,
               curr.count_affiliates_in_country,
               aff.affiliate_max_size,
               aff.affiliate_growth,
               aff.affiliate_tenure_max,
               curr.operating_affiliates_tenure_avg,
               aff.governance_type
        FROM current_affiliates curr
        LEFT JOIN aff_data aff ON (curr.country_code = aff.country_code))
        SELECT aff.country_code,
               aff.count_affiliates_in_country,
               aff.affiliate_max_size,
               aff.affiliate_growth,
               aff.affiliate_tenure_max,
               aff.governance_type,
               aff.operating_affiliates_tenure_avg
           FROM affiliates aff
    """
    )

    df = df.fillna(0)

    # get the schema for the dataframe
    schema_affiliates_leadership = spark.sql(
        f""" select * from {schema}.affiliate_leadership_input_metrics """
    ).schema

    df = df.withColumn("year", F.lit(year))

    df = convert_data_types(df, schema_affiliates_leadership)

    df.write.partitionBy("year").mode("overwrite").saveAsTable(
        f"{schema}.affiliate_leadership_input_metrics"
    )

    spark.stop()


def run():
    """Validates command line arguments and runs the main function"""

    args = _process_args()

    # Get the arguments and pass them to the _main function
    _main(
        schema=args.schema,
        hdfs_path=args.hdfs_path,
        year=args.year,
    )


if __name__ == "__main__":
    run()
