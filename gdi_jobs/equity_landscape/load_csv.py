from gdi_jobs.util import convert_data_types

from pyspark import SparkConf
from pyspark.sql import DataFrame, SparkSession
import pyspark.sql.functions as F
import argparse

"""
Program to load a CSV file from a specified path and write it to Hive
"""


def get_partition_statement(partition_columns: str):
    """Returns a partition statement for a given dataframe and partition column"""
    return (
        "PARTITION ({})".format(partition_columns)
        if partition_columns is not None and partition_columns != ""
        else ""
    )


def handle_partitions(df: DataFrame, partition_columns: str) -> DataFrame:
    """Returns a dataframe with the partition column properly formatted"""
    if partition_columns is not None and partition_columns != "":
        partitions = partition_columns.split(",")
        for partition in partitions:
            partition_column = partition.split("=")[0].strip()
            df = df.drop(partition_column)

    return df


def _insert_into_table(
    table_name: str, partition_statement: str, df: DataFrame, spark: SparkSession
) -> None:
    """Inserts data into a table"""
    df.createOrReplaceTempView("csv_table")

    spark.sql(
        f"""
      INSERT OVERWRITE TABLE {table_name} {partition_statement}
      SELECT *
        FROM csv_table
    """
    )


def _process_args():
    """Processes command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--table_name", help="Name of the table to be written", required=True
    )
    parser.add_argument("--file_location", help="CSV file location", required=True)
    parser.add_argument(
        "--headers",
        help="True/False whether there are headers on the file (denoted by 1 (True) / 0 (False))",
        required=False,
    )
    parser.add_argument(
        "--partition_columns",
        help="A string representation of the partitions to use",
        required=False,
    )
    parser.add_argument("--date_format", help="Date format to be used", required=False)

    args = parser.parse_args()

    return args


def _main(
    table_name: str,
    file_location: str,
    headers: bool,
    partition_columns: str = None,
    date_format: str = None,
) -> None:
    """
    Loads csv file from a specified path and writes it to Hive

    param table_name: Name of the table to be written
    param file_location: CSV file location
    param headers: True/False whether there are headers on the file (denoted by 1 (True) / 0 (False))
    param partition_columns: A string representation of the partitions to use
    param date_format: Date format to be used

    e.g /path/to/load_csv.py --table_name gdi.social_progress_metrics --file_location /path/to/csv/social_progress.csv --partition_columns year='2021' --headers 0 --date_format 'yyyy-MM-dd'
    """

    conf = SparkConf()
    conf.setAppName("load_csv")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    df = spark.sql(f"select * from {table_name}")

    spark.sql("set spark.sql.legacy.timeParserPolicy=LEGACY")

    partition_statement = get_partition_statement(partition_columns)

    df = handle_partitions(df=df, partition_columns=partition_columns)

    schema = df.schema

    df_csv = (
        spark.read.option("sep", "\t")
        .option("header", headers)
        .option("sep", ";")
        .option("sep", ",")
        .option("inferSchema", True)
        .csv(file_location)
    )

    if date_format is None:
        date_format = "yyyy-MM-dd"

    df_csv = convert_data_types(df=df_csv, schema=schema, date_format=date_format)

    _insert_into_table(
        table_name=table_name,
        partition_statement=partition_statement,
        df=df_csv,
        spark=spark,
    )

    spark.stop()


def run():
    """Validates command line arguments and runs the main function"""

    args = _process_args()

    if args.table_name:
        table_name = args.table_name
    else:
        raise ValueError("table_name not specified")

    if args.file_location:
        file_location = args.file_location
    else:
        raise ValueError("file_location not specified")

    if args.headers:
        headers = int(args.headers)
        if headers == 0:
            headers = False
        elif headers == 1:
            headers = True
        else:
            raise ValueError(
                "Error --headers only supports values 1 (True) or 0 (False)"
            )
    else:
        headers = True

    if args.partition_columns:
        partition_columns = args.partition_columns
    else:
        partition_columns = None

    if args.date_format:
        date_format = args.date_format
    else:
        date_format = None

    _main(table_name, file_location, headers, partition_columns, date_format)


if __name__ == "__main__":
    run()
