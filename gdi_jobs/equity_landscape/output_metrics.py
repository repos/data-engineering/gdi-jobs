from pyspark import SparkConf
from pyspark.sql import SparkSession
import argparse


def main(year: int, output_path: str, input_path: str) -> None:
    """
    Takes in the input ranks and writes the output ranks to the output path
    param int year: year
    param str output_path: output path
    param str input_path: input path

    e.g.  /path/to/equity_landscape_input_metrics.py --year 2021 --output_path /path/to/output/ --input_path /path/to/input/

    """

    conf = SparkConf()
    conf.setAppName("equity_landscape_output_metrics")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # Read the input metrics
    spark.read.parquet(input_path).createOrReplaceTempView("input_metrics")

    _readership_metrics(spark)

    _editorship_metrics(spark)

    _affiliate_metrics(spark)

    _grants_metrics(spark)

    _overall_engagement_metrics(spark)

    df = spark.sql(
        f"""
        WITH
            output_ranks AS (
                SELECT * FROM readership_output_ranks
                    UNION ALL
                SELECT * FROM editorship_output_ranks
                    UNION ALL
                SELECT * FROM affiliates_output_ranks
                    UNION ALL
                SELECT * FROM grants_output_ranks
                    UNION ALL
                SELECT * FROM overall_engagement_output_rank
             ),
            output_ranks_excl_global AS (
                SELECT
                    op.*,
                    ip.country_name,
                    ip.sub_continent,
                    ip.continent
                FROM
                    output_ranks op
                LEFT JOIN
                    input_metrics ip
                    ON op.geography_level = ip.geography_level AND op.country_code = ip.iso2_country_code
                WHERE
                    op.geography_level <> 'global'),

            global_ranks AS (
                SELECT
                    'global' AS geography_level,
                    'global' AS country_code,
                    AVG(metric_value) AS metric_value,
                    metric,
                    'global' AS country_name,
                    NULL AS sub_continent,
                    NULL AS continent
                FROM
                    output_ranks_excl_global
                WHERE
                    geography_level = 'continent'
                GROUP BY
                    metric),

            output_ranks_pivot AS (
                SELECT
                    *
                FROM (
                    SELECT * FROM output_ranks_excl_global
                        UNION ALL
                    SELECT * FROM global_ranks)
                PIVOT (
                    SUM(metric_value)
                    FOR
                        metric IN ('reader_presence', 'reader_growth', 'readership',
                                    'editor_presence', 'editor_growth', 'editorship',
                                    'programs_leadership', 'affiliate_presence', 'affiliate_growth',
                                    'affiliates_leadership', 'grants_presence', 'grants_growth',
                                    'grants_leadership', 'overall_engagement', 'population_presence',
                                    'editor_population_penetration', 'percent_global_editors',
                                    'percent_global__active_editors', 'population_underrepresentation_rank',
                                    'access_presence', 'access_growth', 'access_presence_growth'))
                ORDER BY
                    CASE
                        WHEN geography_level = 'country' THEN 1
                        WHEN geography_level = 'sub_continent' THEN 2
                        WHEN geography_level = 'continent' THEN 3
                        WHEN geography_level = 'global' THEN 4
                    END,
                    country_code)

        SELECT *,
               {year} as year
        FROM output_ranks_pivot
    """
    )

    df = df.replace(float("nan"), None)

    df.write.partitionBy("year").mode("overwrite").parquet(output_path)


def _readership_metrics(spark: SparkSession) -> None:
    spark.sql(
        """
        WITH
            readership_input_metrics_hstack AS (
                SELECT
                    geography_level,
                    iso2_country_code AS country_code,
                    COALESCE(pageviews_annual_signal, 0) AS pageviews_annual_signal,
                    COALESCE(pageviews_annual_change, 0) AS pageviews_annual_change,
                    COALESCE(unique_devices_annual_signal, 0) AS unique_devices_annual_signal,
                    COALESCE(unique_devices_annual_change, 0) AS unique_devices_annual_change
                FROM
                    input_metrics
            ),

            readership_input_metrics_vstack AS (
                SELECT
                    geography_level,
                    country_code,
                    STACK(4, pageviews_annual_signal, 'pageviews_annual_signal',
                            pageviews_annual_change, 'pageviews_annual_change',
                            unique_devices_annual_signal, 'unique_devices_annual_signal',
                            unique_devices_annual_change, 'unique_devices_annual_change')
                        AS (metric_value, metric)
                FROM
                    readership_input_metrics_hstack),

            presence_growth_ranks AS (
                SELECT
                    geography_level,
                    country_code,
                    metric,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level, metric
                            ORDER BY metric_value), 3) AS p_rank
                FROM
                    readership_input_metrics_vstack),

            presence AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'reader_presence' AS metric
                    FROM
                        presence_growth_ranks
                    WHERE
                        metric IN ('pageviews_annual_signal', 'unique_devices_annual_signal')
                    GROUP BY
                        geography_level, country_code),

            growth AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'reader_growth' AS metric
                    FROM
                        presence_growth_ranks
                    WHERE
                        metric IN ('pageviews_annual_change', 'unique_devices_annual_change')
                    GROUP BY
                        geography_level, country_code),

            readership_inputs AS (
                SELECT
                    geography_level,
                    country_code,
                    STACK(2, pageviews_annual_signal*pageviews_annual_change, 'pageviews_r',
                            unique_devices_annual_signal*unique_devices_annual_change, 'devices_r') AS (value, input_label)
                FROM
                    readership_input_metrics_hstack),

            readership AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(value))*10, 2) AS metric_value,
                    'readership' AS metric
                    FROM
                        readership_inputs
                    GROUP BY
                        geography_level, country_code)

        SELECT * FROM presence
        UNION ALL
        SELECT * FROM growth
        UNION ALL
        SELECT * FROM readership
        """
    ).createOrReplaceTempView("readership_output_ranks")


def _editorship_metrics(spark):
    spark.sql(
        """
            WITH
                readership_input_metrics_hstack AS (
                    SELECT
                        geography_level,
                        iso2_country_code AS country_code,
                        COALESCE(pageviews_annual_signal, 0) AS pageviews_annual_signal,
                        COALESCE(pageviews_annual_change, 0) AS pageviews_annual_change,
                        COALESCE(unique_devices_annual_signal, 0) AS unique_devices_annual_signal,
                        COALESCE(unique_devices_annual_change, 0) AS unique_devices_annual_change
                    FROM
                        input_metrics
                ),
                readership_input_metrics_vstack AS (
                    SELECT
                        geography_level,
                        country_code,
                        STACK(4, pageviews_annual_signal, 'pageviews_annual_signal',
                                pageviews_annual_change, 'pageviews_annual_change',
                                unique_devices_annual_signal, 'unique_devices_annual_signal',
                                unique_devices_annual_change, 'unique_devices_annual_change')
                            AS (metric_value, metric)
                    FROM
                        readership_input_metrics_hstack),
                presence_growth_ranks AS (
                    SELECT
                        geography_level,
                        country_code,
                        metric,
                        ROUND(
                            PERCENT_RANK() OVER (
                                PARTITION BY geography_level, metric
                                ORDER BY metric_value), 3) AS p_rank
                    FROM
                        readership_input_metrics_vstack),
                presence AS (
                    SELECT
                        geography_level,
                        country_code,
                        ROUND(
                            PERCENT_RANK() OVER (
                                PARTITION BY geography_level
                                ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                        'reader_presence' AS metric
                        FROM
                            presence_growth_ranks
                        WHERE
                            metric IN ('pageviews_annual_signal', 'unique_devices_annual_signal')
                        GROUP BY
                            geography_level, country_code),
                growth AS (
                    SELECT
                        geography_level,
                        country_code,
                        ROUND(
                            PERCENT_RANK() OVER (
                                PARTITION BY geography_level
                                ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                        'reader_growth' AS metric
                        FROM
                            presence_growth_ranks
                        WHERE
                            metric IN ('pageviews_annual_change', 'unique_devices_annual_change')
                        GROUP BY
                            geography_level, country_code),
                readership_inputs AS (
                    SELECT
                        geography_level,
                        country_code,
                        STACK(2, pageviews_annual_signal*pageviews_annual_change, 'pageviews_r',
                                unique_devices_annual_signal*unique_devices_annual_change, 'devices_r') AS (value, input_label)
                    FROM
                        readership_input_metrics_hstack),
                readership AS (
                    SELECT
                        geography_level,
                        country_code,
                        ROUND(
                            PERCENT_RANK() OVER (
                                PARTITION BY geography_level
                                ORDER BY AVG(value))*10, 2) AS metric_value,
                        'readership' AS metric
                        FROM
                            readership_inputs
                        GROUP BY
                            geography_level, country_code)
            SELECT * FROM presence
            UNION ALL
            SELECT * FROM growth
            UNION ALL
            SELECT * FROM readership
    """
    ).createOrReplaceTempView("editorship_output_ranks")

def _grants_metrics(spark):
    spark.sql(
        """
            WITH
                grants_input_metric_hstack AS (
                    SELECT
                        geography_level,
                        iso2_country_code AS country_code,
                        CAST(COALESCE(sum_historical_grants_to_date, 0) AS FLOAT) AS sum_historical_grants_to_date,
                        CAST(COALESCE(sum_calendar_year_grants, 0) AS FLOAT) AS sum_calendar_year_grants,
                        CAST(COALESCE(count_historical_grants_to_date, 0) AS FLOAT) AS count_historical_grants_to_date,
                        CAST(COALESCE(count_calendar_year_grants, 0) AS FLOAT) AS count_calendar_year_grants,
                        CAST(COALESCE(annual_grants_weighted, 0) AS FLOAT) AS annual_grants_weighted,
                        CAST(COALESCE(annual_affiliate_grants_weighted, 0) AS FLOAT) AS annual_affiliate_grants_weighted,
                        CAST(COALESCE(annual_grants_change_rate, 0) AS FLOAT) AS annual_grants_change_rate,
                        CAST(COALESCE(annual_grants_by_annual_change_rate, 0) AS FLOAT) AS annual_grants_by_annual_change_rate,
                        CAST(COALESCE(historical_grants_by_annual_change_rate, 0) AS FLOAT) AS historical_grants_by_annual_change_rate
                    FROM
                        input_metrics),

                grants_input_metric_vstack AS (
                    SELECT
                        geography_level,
                        country_code,
                        STACK(10,
                                sum_historical_grants_to_date, 'sum_historical_grants_to_date',
                                sum_calendar_year_grants, 'sum_calendar_year_grants',
                                count_historical_grants_to_date, 'count_historical_grants_to_date',
                                count_calendar_year_grants, 'count_calendar_year_grants',
                                annual_grants_weighted, 'annual_grants_weighted',
                                annual_affiliate_grants_weighted, 'annual_affiliate_grants_weighted',
                                annual_grants_change_rate, 'annual_grants_change_rate',
                                annual_grants_by_annual_change_rate, 'annual_grants_by_annual_change_rate',
                                historical_grants_by_annual_change_rate, 'historical_grants_by_annual_change_rate') AS (metric_value, metric)
                    FROM
                        grants_input_metric_hstack),

                grants_input_metrics_ranks AS (
                    SELECT
                        geography_level,
                        country_code,
                        metric,
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level, metric
                            ORDER BY metric_value) AS p_rank
                    FROM
                        grants_input_metric_vstack),

                grants_count_rank_avg AS (
                    SELECT
                        geography_level,
                        country_code,
                        'presence_count_rank_average' AS metric,
                        AVG(p_rank) AS p_rank
                    FROM
                        grants_input_metrics_ranks
                    WHERE
                        metric IN ('count_historical_grants_to_date', 'count_calendar_year_grants')
                    GROUP BY
                        geography_level,
                        country_code),

                presence_rank_input AS (
                    SELECT
                        *
                    FROM
                        grants_input_metrics_ranks
                    WHERE
                        metric IN ('annual_grants_weighted', 'annual_affiliate_grants_weighted')
                    UNION ALL
                    SELECT
                        *
                    FROM
                        grants_count_rank_avg),

                presence AS (
                    SELECT
                        geography_level,
                        country_code,
                        ROUND(
                            PERCENT_RANK() OVER (
                                PARTITION BY geography_level
                                ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                        'grants_presence' AS metric
                    FROM
                        presence_rank_input
                    GROUP BY
                        geography_level,
                        country_code),

                growth AS (
                    SELECT
                        geography_level,
                        country_code,
                        ROUND(
                            PERCENT_RANK() OVER (
                                PARTITION BY geography_level
                                ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                        'grants_growth' AS metric
                    FROM
                        grants_input_metrics_ranks
                    WHERE
                        metric IN ('annual_grants_change_rate')
                    GROUP BY
                        geography_level,
                        country_code),

                leadership_rank_input AS (
                    SELECT
                        *
                    FROM
                        grants_input_metrics_ranks
                    WHERE
                        metric IN ('annual_grants_by_annual_change_rate', 'historical_grants_by_annual_change_rate')
                    UNION ALL
                    SELECT
                        *
                    FROM
                        grants_count_rank_avg),

                leadership AS (
                    SELECT
                        geography_level,
                        country_code,
                        ROUND(
                            PERCENT_RANK() OVER (
                                PARTITION BY geography_level
                                ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                        'grants_leadership' AS metric
                    FROM
                        leadership_rank_input
                    GROUP BY
                        geography_level,
                        country_code)


            SELECT * FROM presence
            UNION ALL
            SELECT * FROM growth
            UNION ALL
            SELECT * FROM leadership
    """
    ).createOrReplaceTempView("grants_output_ranks")


def _affiliate_metrics(spark):
    spark.sql(
        """
        WITH
            affiliates_input_metrics_hstack AS (
                SELECT
                    geography_level,
                    iso2_country_code AS country_code,
                    CAST(COALESCE(count_operating_affiliates, 0) AS FLOAT) AS count_operating_affiliates,
                    CAST(COALESCE(affiliate_size_max, 0) AS FLOAT) AS affiliate_size_max,
                    CAST(COALESCE(affiliate_tenure_max, 0) AS FLOAT) AS affiliate_tenure_max,
                    CAST(COALESCE(affiliate_size_growth, 0) AS FLOAT) AS affiliate_size_growth,
                    CAST(COALESCE(affiliate_percent_annual_grants, 0) AS FLOAT) AS affiliate_percent_annual_grants,
                    CAST(COALESCE(affiliate_percent_historical_grants, 0) AS FLOAT) AS affiliate_percent_historical_grants,
                    CAST(COALESCE(historical_affiliate_grants_weighted, 0) AS FLOAT) AS historical_affiliate_grants_weighted,
                    CAST(COALESCE(affiliate_governance_type, 0) AS FLOAT) AS affiliate_governance_type,
                    CAST(COALESCE(affiliate_tenure_average, 0) AS FLOAT) AS affiliate_tenure_average
                FROM
                    input_metrics),
            affiliates_input_metrics_vstack AS (
                SELECT
                    geography_level,
                    country_code,
                    STACK(9,
                        count_operating_affiliates, 'count_operating_affiliates',
                        affiliate_size_max, 'affiliate_size_max',
                        affiliate_tenure_max, 'affiliate_tenure_max',
                        affiliate_percent_annual_grants, 'affiliate_percent_annual_grants',
                        affiliate_percent_historical_grants, 'affiliate_percent_historical_grants',
                        historical_affiliate_grants_weighted, 'historical_affiliate_grants_weighted',
                        affiliate_size_growth, 'affiliate_size_growth',
                        affiliate_tenure_average, 'affiliate_tenure_average',
                        affiliate_governance_type, 'affiliate_governance_type') AS (metric_value, metric)
                FROM
                    affiliates_input_metrics_hstack),
            affiliate_input_metrics_ranks AS (
                SELECT
                    geography_level,
                    country_code,
                    metric,
                    ROUND(
                    PERCENT_RANK() OVER (
                        PARTITION BY geography_level, metric
                        ORDER BY metric_value), 3) AS p_rank
                FROM
                    affiliates_input_metrics_vstack),
            presence AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'affiliate_presence' AS metric
                FROM
                    affiliate_input_metrics_ranks
                WHERE
                    metric IN ('count_operating_affiliates', 'affiliate_size_max', 'affiliate_tenure_max', 'affiliate_tenure_average')
                GROUP BY geography_level, country_code),
            growth AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'affiliate_growth' AS metric
                FROM
                    affiliate_input_metrics_ranks
                WHERE
                    metric IN ('affiliate_percent_annual_grants', 'affiliate_percent_historical_grants', 'affiliate_size_growth')
                GROUP BY geography_level, country_code),
            leadership AS (
                SELECT
                    geography_level,
                    country_code,
                    ROUND(
                        PERCENT_RANK() OVER (
                            PARTITION BY geography_level
                            ORDER BY AVG(p_rank))*10, 2) AS metric_value,
                    'affiliates_leadership' AS metric
                FROM
                    affiliate_input_metrics_ranks
                WHERE
                    metric IN ('count_operating_affiliates', 'affiliate_size_max', 'affiliate_tenure_max',
                                'affiliate_governance_type', 'historical_affiliate_grants_weighted')
                GROUP BY geography_level, country_code)
        SELECT * FROM presence
        UNION ALL
        SELECT * FROM growth
        UNION ALL
        SELECT * FROM leadership
        """
    ).createOrReplaceTempView("affiliates_output_ranks")

def _overall_engagement_metrics(spark):
    spark.sql(
        """
    WITH
        inputs AS (
            SELECT * FROM readership_output_ranks WHERE metric = 'readership'
            UNION ALL
            SELECT * FROM editorship_output_ranks WHERE metric = 'editorship'
            UNION ALL
            SELECT * FROM affiliates_output_ranks WHERE metric = 'affiliates_leadership'
            UNION ALL
            SELECT * FROM grants_output_ranks WHERE metric = 'grants_leadership')
    SELECT
        geography_level,
        country_code,
        ROUND(
            PERCENT_RANK() OVER (
                PARTITION BY geography_level
                ORDER BY AVG(metric_value))*10, 2) AS metric_value,
        'overall_engagement' AS metric
    FROM
        inputs
    GROUP BY geography_level, country_code
    """
    ).createOrReplaceTempView("overall_engagement_output_rank")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--year", type=str, help="Year for which metrics should be calculated for"
    )
    parser.add_argument("--output-path", type=str, help="Path to write output to")

    parser.add_argument("--input-path", type=str, help="Path to read input from")

    args = parser.parse_args()

    if args.year:
        year = int(args.year)
    else:
        raise ValueError("Year not specified")

    if args.output_path:
        output_path = args.output_path
    else:
        raise ValueError("Output path not specified")

    if args.input_path:
        input_path = args.input_path
    else:
        raise ValueError("Input path not specified")

    input_path = f"{input_path}/year={year}"

    main(year=year, output_path=output_path, input_path=input_path)
