from typing import Generator, List

import luadata
from pyspark.sql import DataFrame
import pyspark.sql.functions as F
from pyspark.sql.types import StructType
import wbgapi as wb

from mediawiki import MediaWiki

proxies = {
    "http": "http://webproxy.eqiad.wmnet:8080",
    "https": "http://webproxy.eqiad.wmnet:8080",
}


# takes in a proxy dict to allow http calls to be made from airflow
def set_proxy(proxy_dict: dict = proxies):
    """
    param dict proxy_dict: Dictionary representing the proxy details to be used
    """
    wb.proxies = proxy_dict


# fetch data from World Bank API and returns a generator You might get timeouts if the proxy is not set, so be sure
# to set the proxy first before calling fetch_data unless if calling on your local machine.
def fetch_data(db: int, series: str) -> Generator:
    """
    param int db: number denoting an indicator in the WorldBank database
    param str series: Series name used to denote KPI in the WorldBank database
    returns a Generator

    kpi_name	                                        series_identifier	db_id
    Control of Corruption	                            CC.EST	            3
    Mobile cellular subscriptions (per 100 people)	    IT.CEL.SETS.P2	    2
    Individuals using the Internet (% of population)	IT.NET.USER.ZS	    2
    Population	                                        SP.POP.TOTL	        2
    GDP, PPP (current international $)	                NY.GDP.MKTP.PP.CD	2
    GDP, PPP (constant 2017 international $)	        NY.GDP.MKTP.PP.KD	2
        World Bank Gender Ratios	                        SP.POP.TOTL.FE.ZS	2
    """

    return wb.data.fetch(series=series, db=db)


# Fetch lua table page using the mediawiki api and parse the data into a list of dicts
def fetch_lua_data(page_name: str) -> List[dict]:
    """
    param str page_name: name of the lua table page
    returns list of dicts
    """
    mw = MediaWiki(lang="meta")
    page = mw.page(page_name)
    return luadata.unserialize(page.wikitext, encoding="utf-8", multival=False)


def rename_columns(df: DataFrame, schema) -> DataFrame:
    """Renames columns to match the schema"""
    return df.toDF(*[field.name for field in schema.fields])


def handle_data_types(df: DataFrame, date_format: str) -> DataFrame:
    """Handles data types for the dataframe"""
    for column_name, column_type in df.dtypes:
        df = _handle_data_type(df, date_format, column_name, column_type)

    return df


def _handle_data_type(df, date_format, column_name, column_type):
    if "array" in column_type:
        df = df.withColumn(column_name, F.split(column_name, ","))

    elif "date" in column_type:
        df = df.withColumn(
            column_name, F.expr(f"to_date({column_name},'{date_format}')")
        )

    elif _is_numeric(column_type):
        df = df.withColumn(column_name, F.col(column_name).cast(column_type))
    return df


def _is_numeric(column_type):
    return (
        "decimal" in column_type
        or "int" in column_type
        or "long" in column_type
        or "float" in column_type
        or "double" in column_type
    )


def convert_data_types(
    df: DataFrame, schema: StructType, date_format: str = "YYYY-mm-dd"
) -> DataFrame:
    """Converts data types for the dataframe based on the schema"""

    # rename columns to match the schema
    df = rename_columns(df, schema)

    for schema in schema.fields:

        column_name, column_type = schema.simpleString().split(":")

        df = _handle_data_type(df, date_format, column_name, column_type)

    return df
