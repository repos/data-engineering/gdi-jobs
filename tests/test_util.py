from gdi_jobs.util import fetch_data


# Tests the fetch_data function to see if it returns any data when invoked
def test_fetch_data():
    assert next(fetch_data(2, 'IT.NET.USER.ZS')) is not None

