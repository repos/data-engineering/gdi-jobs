from gdi_jobs.equity_landscape.load_csv import get_partition_statement


def test_get_partition_statement():
    assert get_partition_statement(partition_columns="year='2021'") == "PARTITION (year='2021')"


def test_get_partition_statement_multiple():
    assert get_partition_statement(partition_columns="year='2021',month='01'") == "PARTITION (year='2021',month='01')"


def test_get_partition_statement_empty():
    assert get_partition_statement(partition_columns="") == ""